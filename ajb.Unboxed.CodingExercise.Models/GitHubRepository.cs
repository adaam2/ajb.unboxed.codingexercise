﻿using Newtonsoft.Json;
using System;

namespace ajb.Unboxed.CodingExercise.Models
{
    public class GitHubRepository
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("html_url")]
        public string Url { get; set; }
        [JsonProperty("created_at")]
        public DateTime Created { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
