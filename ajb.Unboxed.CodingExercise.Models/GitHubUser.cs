﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ajb.Unboxed.CodingExercise.Models
{
    public class GitHubUser
    {
        [JsonProperty("login")]
        public string UserName { get; set; }

        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }

        [JsonProperty("html_url")]
        public string ProfileUrl { get; set; }

    }
}
