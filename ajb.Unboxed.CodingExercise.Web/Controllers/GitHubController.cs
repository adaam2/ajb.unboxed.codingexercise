﻿using ajb.Unboxed.CodingExercise.GitHub.Interfaces;
using System.Web.Http;

namespace ajb.Unboxed.CodingExercise.Web.Controllers
{
    public class GitHubController : ApiController
    {
        private readonly IGitHubService _gitHubService;

        public GitHubController(IGitHubService gitHubService)
        {
            _gitHubService = gitHubService;
        }

        [HttpGet]
        public IHttpActionResult GetUserNameSuggestions(string searchText)
        {
            var suggestions = _gitHubService.GetUserNameSuggestions(searchText);
            return Ok(suggestions);
        }

        [HttpPost]
        public IHttpActionResult GetFavouriteProgrammingLanguage(string userName)
        {
            var topProgrammingLanguage = _gitHubService.GetBestGuessFavouriteProgrammingLanguage(userName);

            if(topProgrammingLanguage.Value.Count < 1)
            {
                return BadRequest("No results found for this username");
            }
            return Ok(topProgrammingLanguage);
        }
    }
}
