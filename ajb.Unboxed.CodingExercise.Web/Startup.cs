﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Newtonsoft.Json;
using Ninject;
using ajb.Unboxed.CodingExercise.Web.Ninject;
using System.Web.Mvc;
using System.Reflection;
using RestSharp;
using ajb.Unboxed.CodingExercise.GitHub.Interfaces;
using ajb.Unboxed.CodingExercise.GitHub;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;

[assembly: OwinStartup(typeof(ajb.Unboxed.CodingExercise.Web.Startup))]

namespace ajb.Unboxed.CodingExercise.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            // remove xml
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            //// Web API routes
            //config.Routes.MapHttpRoute(
            //    name: "GitHub",
            //    routeTemplate: "github",
            //    defaults: new { controller = "GitHub", action = "GetFavouriteProgrammingLanguage" }
            //);

            // Map this rule first
            //config.Routes.MapHttpRoute(
            //     "WithActionApi",
            //     "api/{controller}/{action}/{id}"
            // );

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = System.Web.Http.RouteParameter.Optional }
            );

            // Prevent "Self referencing loop detected" error occurring for recursive objects
            var serializerSettings = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                StringEscapeHandling = StringEscapeHandling.Default,
                NullValueHandling = NullValueHandling.Ignore
            };

            // Set the default JSON.NET serializer settings
            config.Formatters.JsonFormatter.SerializerSettings = serializerSettings;
            System.Web.Http.GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings = serializerSettings;

            // Use the ninject middleware and ninject web api middleware
            app.UseNinjectMiddleware(CreateKernel).UseNinjectWebApi(config);
        }

        private static StandardKernel CreateKernel()
        {
            // Instantite a new StandardKernel and resolver.
            StandardKernel kernel = new StandardKernel();
            NinjectDependencyResolver resolver = new NinjectDependencyResolver(kernel);

            // Set the dependency resolvers for both MVC and Web API controllers
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = resolver; // Web API
            DependencyResolver.SetResolver(resolver); // MVC

            // Load the classes from the executing assembly.
            kernel.Load(Assembly.GetExecutingAssembly());

            // Register all of the Ninject bindings so that they can be injected into controllers
            RegisterServices(kernel);

            // Return the kernel instance to the API middleware registration call
            return kernel;
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IGitHubService>().To<GitHubService>();

        }
    }
}
