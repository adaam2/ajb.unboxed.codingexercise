﻿'use strict';

app.factory('gitHubService', ['$http', function ($http) {
    var serviceBase = '/api/';
    var gitHubServiceFactory = {};

    gitHubServiceFactory.getUserNameSuggestions = function (searchText, callback) {
        $http({
            method: 'GET',
            url: serviceBase + 'GitHub/GetUserNameSuggestions?searchText=' + searchText,
            withCredentials: false,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .success(function (data, status, headers, config) {
            // on success
            callback(data, status);
        })
        .error(function (data, status, headers, config) {
            // on error
            callback(data, status);
        });
    }

    gitHubServiceFactory.getTopProgrammingLanguageForUserName = function (userName, callback) {
        $http({
            method: 'POST',
            url: serviceBase + 'GitHub/GetFavouriteProgrammingLanguage?userName=' + userName,
            withCredentials: false,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .success(function (data, status, headers, config) {
            // on success
            callback(data, status);
        })
        .error(function (data, status, headers, config) {
            // on error
            callback(data, status);
        });
    }
    return gitHubServiceFactory;

}]);