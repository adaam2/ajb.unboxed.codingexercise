﻿var app = angular.module('ajb.Unboxed.CodingExercise', ['ngRoute'])
.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
          templateUrl: 'Scripts/app/views/home.html',
          controller: 'homeController'
      })
      .otherwise({
          redirectTo: '/'
      });
})
.filter('urlEncode', [function () {
    return window.encodeURIComponent;
}]);