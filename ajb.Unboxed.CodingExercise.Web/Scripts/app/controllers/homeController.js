﻿'use strict';
app.controller('homeController', ['$scope', '$timeout', 'gitHubService', function ($scope, $timeout, gitHubService) {
    $scope.user = '';
    $scope.programmingLanguageLoading = false;
    $scope.noRepositoriesFound = true;
    $scope.entries = null;

    $scope.getTopProgrammingLanguageForUserName = function (userName) {
        $scope.user = userName;
        $scope.programmingLanguageLoading = true;

        gitHubService.getTopProgrammingLanguageForUserName(userName, function (data, status) {
            if (status === 200) {
                $scope.topProgrammingLanguageResult = data.Key;
                $scope.repositories = data.Value;
                $scope.noRepositoriesFound = false;
            }
            else
            {
                $scope.topProgrammingLanguageResult = "This user has no repositories!";
                $scope.noRepositoriesFound = true;
            }
            $scope.programmingLanguageLoading = false;
        });
        
    };

    $scope.$watch('userName', function (tmpStr) {
        if (!tmpStr || tmpStr.length == 0)
            return 0;
        $timeout(function () {
            
            gitHubService.getUserNameSuggestions($scope.userName, function (data, status) {
                if (status === 200) {
                    $scope.entries = data;
                }
            });
            
        }, 200);
       
    });
}]);