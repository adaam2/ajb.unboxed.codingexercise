﻿using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Resolvers;
using BundleTransformer.Core.Transformers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ajb.Unboxed.CodingExercise.Web.App_Start
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            ScriptBundle vendor = new ScriptBundle("~/bundles/vendor");
            vendor.Include("~/Scripts/angular.min.js");
            vendor.Include("~/Scripts/angular-route.min.js");

            bundles.Add(vendor);

            ScriptBundle spaBundle = new ScriptBundle("~/bundles/spa");
            spaBundle.Include("~/Scripts/app/app.js");
            spaBundle.Include("~/Scripts/app/controllers/homeController.js");
            spaBundle.Include("~/Scripts/app/services/gitHubService.js");

            bundles.Add(spaBundle);

            // Replace a default bundle resolver in order to the debugging HTTP-handler
            // can use transformations of the corresponding bundle
            BundleResolver.Current = new CustomBundleResolver();

            var nullBuilder = new NullBuilder();
            var styleTransformer = new StyleTransformer();
            var scriptTransformer = new ScriptTransformer();
            var nullOrderer = new NullOrderer();

            StyleBundle styles = new StyleBundle("~/bundles/styles");
            styles.Include("~/Scripts/bower_components/");
            styles.Include("~/Styles/main.scss");
            styles.Builder = nullBuilder;
            styles.Transforms.Add(styleTransformer);
            styles.Transforms.Add(new CssMinify());
            styles.Orderer = nullOrderer;
            bundles.Add(styles);

            BundleTable.EnableOptimizations = false;
        }
    }
}