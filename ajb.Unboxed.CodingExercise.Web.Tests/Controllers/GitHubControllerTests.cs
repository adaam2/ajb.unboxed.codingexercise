﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ajb.Unboxed.CodingExercise.Web.Controllers;
using Moq;
using ajb.Unboxed.CodingExercise.GitHub.Interfaces;
using System.Web.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;

namespace ajb.Unboxed.CodingExercise.Web.Tests
{
    [TestClass]
    public class GitHubControllerTests
    {
        private GitHubController controller;

        [TestInitialize]
        public void Initialize()
        {
            var mockedGitHubService = new Mock<IGitHubService>();

            controller = new GitHubController(mockedGitHubService.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
        }

        [TestMethod]
        public async Task GetFavouriteProgrammingLanguage_Should_Not_Return_Empty()
        {
            // Arrange
            string empty = string.Empty;

            var cancellationToken = new CancellationToken();

            // Act
            var responseMessage = await controller.GetFavouriteProgrammingLanguage("adaam2").ExecuteAsync(cancellationToken);

            // Assert
            Assert.AreNotEqual(empty, responseMessage.Content);
        }
    }
}
