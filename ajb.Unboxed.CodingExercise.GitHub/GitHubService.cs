﻿using ajb.Unboxed.CodingExercise.GitHub.Interfaces;
using System;
using RestSharp;
using System.Collections.Generic;
using ajb.Unboxed.CodingExercise.Models;
using ajb.Unboxed.CodingExercise.Models.DTOs;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using System.Linq;

namespace ajb.Unboxed.CodingExercise.GitHub
{
    public class GitHubService : IGitHubService
    {
        private readonly IRestClient _restClient;

        private const string _clientId = "c40c3bb0700d5aaaa65b";

        private const string _clientSecret = "22db2b8e679bbc41121e4a492d2f4e584cf528ec";

        private const int _perPage = 100;

        public GitHubService()
        {
            string gitHubBaseUrl = "https://api.github.com";

            _restClient = new RestClient(gitHubBaseUrl);
        }

        public KeyValuePair<string, List<GitHubRepository>> GetBestGuessFavouriteProgrammingLanguage(string gitHubUserName)
        {
            var req = new RestRequest(Method.GET);
            req.Resource = string.Format("/users/{0}/repos?client_id={1}&client_secret={2}&per_page={3}", gitHubUserName, _clientId, _clientSecret, _perPage);
            req.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            req.AddHeader("User-Agent", "adaam2");

            IRestResponse response = GetApiResponse(req);

            IList<GitHubRepository> userRepositories = ConvertResponseToList<GitHubRepository>(response);

            Dictionary<string, List<GitHubRepository>> dictionary = new Dictionary<string, List<GitHubRepository>>();

            if(userRepositories.Count() > 0)
            {
                foreach (GitHubRepository repo in userRepositories)
                {
                    string mainProgrammingLanguage = repo.Language;

                    if (!string.IsNullOrEmpty(mainProgrammingLanguage))
                    {
                        if (dictionary.ContainsKey(mainProgrammingLanguage))
                        {
                            dictionary[mainProgrammingLanguage].Add(repo);
                        }
                        else
                        {
                            dictionary.Add(mainProgrammingLanguage, new List<GitHubRepository>() { repo });
                        }
                    }
                }
            }
            return dictionary
                .OrderByDescending(i => i.Value.Count)
                .ToDictionary(d => d.Key, d => d.Value.ToList())
                .FirstOrDefault();
        }
        private IRestResponse GetApiResponse(RestRequest request)
        {
            return _restClient.Execute(request);
        }

        private RootObject ConvertResponseToRootObject(IRestResponse response)
        {
            try
            {
                return JsonConvert.DeserializeObject<RootObject>(response.Content);
            }
            catch
            {
                return new RootObject();
            }
        }

        private IList<T> ConvertResponseToList<T>(IRestResponse response)
        {
            try
            {
                // Try and deserialize the response from GitHub to a generic collection of type T
                var list = JsonConvert.DeserializeObject<IList<T>>(response.Content);

                return list;
            }
            catch
            {
                // Return an empty list
                return new List<T>();
            }            
        }

        public IList<GitHubUser> GetUserNameSuggestions(string searchText)
        {
            var req = new RestRequest(Method.GET);
            req.Resource = string.Format("/search/users?q={0}&client_id={1}&client_secret={2}&per_page={3}", searchText, _clientId, _clientSecret, 5);
            req.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            req.AddHeader("User-Agent", "adaam2");

            var response = GetApiResponse(req);

            RootObject rootObject = ConvertResponseToRootObject(response);

            IList<GitHubUser> users = new List<GitHubUser>();

            if (rootObject.total_count > 0)
            {
                // Take the results, order by the relevancy score in a descending manner and return the top 5 as the target User model
                users = rootObject.items
                                            .OrderByDescending(o => o.score)
                                            .Select(o => new GitHubUser()
                                            {
                                                AvatarUrl = o.avatar_url,
                                                ProfileUrl = o.html_url,
                                                UserName = o.login
                                            })
                                            .ToList();
            }

            return users;
        }
    }
}
