﻿using ajb.Unboxed.CodingExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ajb.Unboxed.CodingExercise.GitHub.Interfaces
{
    public interface IGitHubService
    {
        KeyValuePair<string, List<GitHubRepository>> GetBestGuessFavouriteProgrammingLanguage(string gitHubUserName);

        IList<GitHubUser> GetUserNameSuggestions(string searchText);
    }
}
